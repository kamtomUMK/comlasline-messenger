//
//  BCCNewAccountViewController.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 17.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCCNewAccountViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *wholeview;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *Name;
@property (weak, nonatomic) IBOutlet UILabel *ErrorLabel;
@property (weak, nonatomic) IBOutlet UIButton *CNAButton;
- (IBAction)createAccount:(id)sender;
-(void)createAccountResult:(id)resp;
@end
