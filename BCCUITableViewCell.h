//
//  BCCUITableViewCell.h
//  CompassComunicator
//
//  Created by Kamil Tomasiak on 24.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCCPerson.h"

@interface BCCUITableViewCell : UITableViewCell

@property BCCPerson *person;
@property int index;
@property int numberOfLines;

@end
