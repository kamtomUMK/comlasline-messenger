//
//  BCCAnnotationView.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 21.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "BCCPerson.h"

@interface BCCAnnotationView : MKAnnotationView
-(id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier AndPerson:(BCCPerson *)person andImgName:(NSString *)imgName;
@end
