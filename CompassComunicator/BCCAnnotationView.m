//
//  BCCAnnotationView.m
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 21.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCAnnotationView.h"
#import "BCCPerson.h"
#import "BCCButton.h"
#import "BCCLoggedUser.h"

@implementation BCCAnnotationView
-(id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier AndPerson:(BCCPerson *)person andImgName:(NSString *)imgName{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        CGSize nameAndSurnameSize = [person.sName sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Arial" size:14]}];
        NSLog(@"person: %@ size: %f", person.sName, nameAndSurnameSize.width);
        
        CGRect  myFrame = self.frame;
        myFrame.size.width = 300;
        myFrame.size.height = 75;
        self.frame = myFrame;
        self.canShowCallout = YES;
        self.image = [UIImage imageNamed:imgName];
        self.opaque = YES;
        
        UIView *uiLeft;
        if(nameAndSurnameSize.width+45 > 90) {
            uiLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45+nameAndSurnameSize.width-20, 50)];
        }
        else {
            uiLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 50)];
        }
        
        BCCButton *profilButton = [BCCButton buttonWithType:UIButtonTypeCustom];
        profilButton.person = person;
        profilButton.frame = CGRectMake(45, 25, 15, 15);
        [profilButton setImage:[UIImage imageNamed:@"compas_profile.png"] forState:UIControlStateNormal];
        [profilButton addTarget:self action:@selector(showUserProfil:) forControlEvents:UIControlEventTouchDown];
      
        UILabel *nameAndSurname;
        nameAndSurname = [[UILabel alloc] initWithFrame:CGRectMake(45, 5, nameAndSurnameSize.width, nameAndSurnameSize.height)];
        nameAndSurname.font = [UIFont fontWithName:@"Arial" size:14.0];
        [nameAndSurname setText:person.sName];
        
        UIImageView *avatar = [person getMyPicture];
        [uiLeft addSubview:avatar];
        
        [uiLeft addSubview:profilButton];
        [uiLeft addSubview:nameAndSurname];
        
        BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
        
        
        BCCButton* button;
        if(![loggedUser isMyFriend:person]) {
            button = [self createButton:person withImageName:@"addFriend.png" withRect:CGRectMake(65, 25, 15, 15)];
            [button addTarget:self action:@selector(sendInvitationToUser:) forControlEvents:UIControlEventTouchDown];
        } else {
            [uiLeft addSubview:[self createButton:person withImageName:@"messageOffline.png" withRect:CGRectMake(65, 25, 15, 15)]];
        }
        [uiLeft addSubview: button];
        
        BCCButton *conversationButton = [BCCButton buttonWithType:UIButtonTypeCustom];
        conversationButton.person = person;
        conversationButton.frame = CGRectMake(80, 25, 15, 15);
        [conversationButton setImage:[UIImage imageNamed:@"message.png"] forState:UIControlStateNormal];
        //[conversationButton addTarget:self action:@selector(showUserProfil:) forControlEvents:UIControlEventTouchDown];
        // TODO rozpoczyna rozmowe online
        
        
        self.leftCalloutAccessoryView = uiLeft;
    }
    return self;
}

-(BCCButton*) createButton: (BCCPerson*) person withImageName:(NSString*)name withRect:(CGRect)rect{
    BCCButton *button = [BCCButton buttonWithType:UIButtonTypeCustom];
    button.frame = rect;
    button.person = person;
    [button setImage:[UIImage imageNamed:name] forState:UIControlStateNormal];
    return button;
}

-(void) showUserProfil:(id) sender {
    BCCPerson *person = ((BCCButton*)sender).person;
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.compas.org.pl/web/%@", person.sName];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(void) sendInvitationToUser:(id) sender {
    BCCPerson *person = ((BCCButton*)sender).person;
    BCCLoggedUser *loggedUser =[BCCLoggedUser getInstance];
    
    NSString *format = @"http://www.compas.org.pl/OkienkoCompasy/ajax.jsp?l=load&action=addFriend&on=%i&ja=%i";
    NSString *ursl = [NSString stringWithFormat:format, person.userId, loggedUser.myPersonInstance.userId];
    NSData *apiResponse = [NSData dataWithContentsOfURL:[NSURL URLWithString:ursl]];
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:apiResponse options:nil error:nil];
    
    //TODO zamiana obeznego buttona na inny do czasu potwierdzenia zaproszenia
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{}


@end
