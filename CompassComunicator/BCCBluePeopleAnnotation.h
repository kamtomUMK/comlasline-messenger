//
//  BCCBluePeopleAnnotation.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "BCCPeopleAnnotation.h"

@interface BCCBluePeopleAnnotation : BCCPeopleAnnotation
@end
