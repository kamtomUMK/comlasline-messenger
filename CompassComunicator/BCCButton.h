//
//  BCCButton.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCCPerson.h"

@interface BCCButton : UIButton {
    BCCPerson * person_;
}

@property (nonatomic, retain) BCCPerson * person;
- (id) initWithPerson: (BCCPerson *) person;


@end
