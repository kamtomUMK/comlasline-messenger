//
//  BCCButton.m
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCButton.h"

@implementation BCCButton
@synthesize person = person_;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"INICJALIZACJA");
        // Initialization code
    }
    return self;
}

- (id) initWithPerson: (BCCPerson *) person
{
    self = [super init];
    if (self) {
        // Initialization code
        self.person = person;
    }
    
    return self;
}

@end
