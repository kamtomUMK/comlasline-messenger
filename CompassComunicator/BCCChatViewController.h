//
//  BCCChatTableViewController.h
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 21.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCCPerson.h"

@interface BCCChatViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIView *allView;
@property (weak, nonatomic) IBOutlet UITextField *messageField;
@property NSMutableArray* listOfUserMessage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *cellsHeights;
- (IBAction)startEditing:(id)sender;
@property BCCPerson *personWithIAmTalkingTo;
@end
