//
//  BCCChatTableViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 21.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCChatViewController.h"
#import "BCCMenuBarViewController.h"
#import "BCCUITableViewCell.h"
#import "BCCLoggedUser.h"
#import "BCCPerson.h"
#import "BCCJSONConnector.h"

@interface BCCChatViewController ()

@end

@implementation BCCChatViewController

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void) viewDidAppear:(BOOL)animated
{

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.listOfUserMessage = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onGoingIntoChatView:)
     name: _N_intoChatView
     object: nil ];
    
    [self.messageField setReturnKeyType:UIReturnKeyDone];
    self.messageField.delegate = self;
}

-(void) onGoingIntoChatView: (NSNotification*)notify {
    
    BCCPerson *person = (BCCPerson *)[notify valueForKey:@"object"];
    self.personWithIAmTalkingTo = person;
    
    self.cellsHeights = [[NSMutableArray alloc] init];
    
    BCCLoggedUser * user = [BCCLoggedUser getInstance];
    self.listOfUserMessage = [user getMyMessageWithUser: person.userId];
    
    [self.tableView reloadData];
    [self scrollToBottom];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
   // NSLog(@"COUNt: %i", self.listOfUserMessage.count);
    return self.listOfUserMessage.count;
}

-(void) scrollToBottom {
    NSIndexPath* ipath = [NSIndexPath indexPathForRow: self.listOfUserMessage.count-1 inSection: 0];
    [self.tableView scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGRect sizeRect = [[UIScreen mainScreen] bounds];
    NSDictionary *person = [self.listOfUserMessage objectAtIndex:indexPath.row];
    CGSize messageSize = [[person valueForKey:@"wpis" ] sizeWithAttributes: @{NSFontAttributeName: [UIFont fontWithName:@"Arial" size:10]}];
    
    int numberOfLines = messageSize.width / (sizeRect.size.width-45);
    NSNumber *size = [NSNumber numberWithInt:30+(10*(numberOfLines))];
    [self.cellsHeights addObject:size];
    
    return 30+(10*(numberOfLines));
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *msg = [self.listOfUserMessage objectAtIndex:indexPath.row];
    BCCLoggedUser * user = [BCCLoggedUser getInstance];
    
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    int heightCell = [[self.cellsHeights objectAtIndex:indexPath.row] intValue];
    
    UIImageView *avatar;
    UILabel *message;
    CGRect sizeRect = [[UIScreen mainScreen] bounds];
    
    if ( [[msg valueForKey:@"u_nadawca"] intValue] == user.myPersonInstance.userId) {
        [cell setFrame: CGRectMake(0, 10, sizeRect.size.width, 75)];
        
        message = [[UILabel alloc] initWithFrame:CGRectMake(45, 0, sizeRect.size.width-45, heightCell)];
        message.font = [UIFont fontWithName:@"Arial" size:10];
        message.lineBreakMode = NSLineBreakByWordWrapping;
        message.numberOfLines = 0;
        message.text = [msg valueForKey:@"wpis"];

        avatar = [user.myPersonInstance getMyPicture];
        avatar.frame = CGRectMake(5,(heightCell-30)/2,30,30);
    } else {
        [cell setFrame: CGRectMake(0, 10, 100, 75)];
        
        message = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, sizeRect.size.width-45, heightCell)];
        message.textAlignment = NSTextAlignmentRight;
        message.font = [UIFont fontWithName:@"Arial" size:10];
        message.lineBreakMode = NSLineBreakByWordWrapping;
        message.numberOfLines = 0;
        message.text = [msg valueForKey:@"wpis"];
        
        BCCJSONConnector * jsonConnector =  [BCCJSONConnector getInstance];
        avatar = [((BCCPerson *)[jsonConnector getUserById: [[msg valueForKey:@"u_nadawca"]intValue]]) getMyPicture];
        avatar.frame = CGRectMake(sizeRect.size.width-45+5, (heightCell-30)/2,30,30);
    }
    [cell addSubview: avatar];
    [cell addSubview: message];

    return cell;
}

-(void) addNewMessage {
    BCCLoggedUser *loggeUser = [BCCLoggedUser getInstance];

    NSString *myId = [NSString stringWithFormat:@"%i", loggeUser.myPersonInstance.userId];
    NSString *message = self.messageField.text;

    NSDictionary *newMessage = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:myId, message, nil] forKeys:[NSArray arrayWithObjects:@"u_nadawca", @"wpis", nil]];
    
    [self.listOfUserMessage addObject:newMessage];
    
    //perform on new thread to avoid the UI from freezing
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                   ^{
                     [self.tableView reloadData];
                   });
    
}

- (IBAction)startEditing:(id)sender {
    [UIView animateWithDuration:1.0 animations:^{
        self.allView.center=CGPointMake(160, self.view.center.y-150);
    }];
}

- (IBAction)endEditing:(id)sender {
    [self.messageField resignFirstResponder];
    [UIView animateWithDuration:1.0 animations:^{
        self.allView.center=CGPointMake(160, self.view.center.y+150);
    }];
}

- (IBAction)sendMessage:(id)sender {
    NSString *message = self.messageField.text;
    
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    [jsonConnector sendMessage:message toUser:self.personWithIAmTalkingTo.userId];
    
    [self addNewMessage];
    [self scrollToBottom];
}

@end
