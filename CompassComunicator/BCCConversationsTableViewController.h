//
//  BCCConversationsTableViewController.h
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 21.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCCConversationsTableViewController : UITableViewController

@property NSMutableArray* listOfConverationPeopleCell;

@end
