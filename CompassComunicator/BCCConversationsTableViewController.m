//
//  BCCConversationsTableViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 21.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCConversationsTableViewController.h"
#import "BCCMenuBarViewController.h"
#import "BCCLoggedUser.h"
#import "BCCUITableViewCell.h"

@interface BCCConversationsTableViewController ()
@property NSMutableArray *activeWindows;
@end

@implementation BCCConversationsTableViewController

-(id) init {
    self = [super init];
    
    
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    
    if (self) {}
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName: _N_newViewDidAppear object:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.listOfConverationPeopleCell = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    self.activeWindows = [loggedUser getMyActiveChatPersonWindows];
    
    return self.activeWindows.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BCCUITableViewCell *cell = [[BCCUITableViewCell alloc] init];
    
    [cell setFrame: CGRectMake(0, 10, 100, 30)];
    int i = [indexPath indexAtPosition:1];
    BCCPerson *person = (BCCPerson*)self.activeWindows[i];
    UILabel *nameAndSurname = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, 100, 10)];
    nameAndSurname.font = [UIFont fontWithName:@"Arial" size:10];
    nameAndSurname.text = [person login];
    
    cell.person = person;
    cell.index = i;
    
    UIImageView *avatar = [person getMyPicture];
    avatar.frame = CGRectMake(0,0,30,30);
    
    [cell addSubview: avatar];
    [cell addSubview: nameAndSurname];
    
    [self.listOfConverationPeopleCell addObject:cell];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    BCCPerson *selectPerson = ((BCCUITableViewCell*)[self.listOfConverationPeopleCell objectAtIndex:indexPath.row]).person;
    
 //   [self performSegueWithIdentifier:@"intoChatTableView" sender:self];
    [[NSNotificationCenter defaultCenter]postNotificationName:_N_intoChatView object:selectPerson];
}

@end
