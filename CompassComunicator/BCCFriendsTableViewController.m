//
//  BCCFriendsTableViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCFriendsTableViewController.h"
#import "BCCLoggedUser.h"
#import "BCCPersonsViewController.h"

@interface BCCFriendsTableViewController ()

@property NSArray * MyActiveFriends;
@property bool isSegueAllowed;

@end

@implementation BCCFriendsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_TableViewDidAppear object:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onGoingIntoFriendsView:)
     name: _N_intoFriendsTableView
     object: nil ];
    
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onGoingIntoOthersView:)
     name: _N_intoOthersTableView
     object: nil ];
    
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onGoingIntoInvitesView:)
     name: _N_intoInvitesTableView
     object: nil ];
    
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onTableViewDidAppear:)
     name: _N_TableViewDidAppear
     object: nil ];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void) onGoingIntoFriendsView:(NSNotification *)notify
{
    if([self isSegueAllowed] && [[self navigationController] visibleViewController] != self)
    {
        [self setIsSegueAllowed: false];
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
}

- (void) onGoingIntoOthersView:(NSNotification *)notify
{
    if([self isSegueAllowed])
    {
        [self setIsSegueAllowed: false];
        
        //[[self navigationController] popToRootViewControllerAnimated:YES];
        [self performSegueWithIdentifier:@"intoOthersTableView" sender:self];
    }
}

- (void) onGoingIntoInvitesView:(NSNotification *)notify
{
    if([self isSegueAllowed])
    {
        [self setIsSegueAllowed: false];
        
        //[[self navigationController] popToRootViewControllerAnimated:YES];
        [self performSegueWithIdentifier:@"intoInvitesTableView" sender:self];
    }
}
- (void) onTableViewDidAppear:(NSNotification *)notify
{
    [self setIsSegueAllowed: true];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    self.MyActiveFriends = [[BCCLoggedUser getInstance] getMyActiveFriends];
    return self.MyActiveFriends.count;
    // Return the number of rows in the section.
    //return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];*/
    
    // Configure the cell...
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    
    [cell setFrame: CGRectMake(0,0,100, 30)];
    int i = [indexPath indexAtPosition:1];
    BCCPerson* person = ((BCCPerson*)self.MyActiveFriends[i]);
    UILabel *lab =[[UILabel alloc]initWithFrame:CGRectMake(45, 0, 100, 30)];
    lab.font = [UIFont fontWithName:@"Arial" size:10];
    lab.text = [person login];
    
    UIImageView *avatar = [person getMyPicture];
    avatar.frame = CGRectMake(10,0,30,30);
    
    [cell addSubview: avatar];
    [cell addSubview: lab];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
