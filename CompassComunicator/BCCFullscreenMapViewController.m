//
//  BCCFullscreenMapViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCFullscreenMapViewController.h"
#import "BCCMenuBarViewController.h"

@interface BCCFullscreenMapViewController ()
@property (weak, nonatomic) IBOutlet UIView *MapView;
@property (weak, nonatomic) IBOutlet UIView *RightBar;
@property (weak, nonatomic) IBOutlet UIView *ChatView;

@end

@implementation BCCFullscreenMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        NSLog(@"Dodaje widok podzielony");
    }
    return self;
}


/**
 *
 * Przesuwa tabelke z listą w lewo na ekran.
 *
 */
- (void)onGoingIntoPersonsView:(NSNotification *)notif
{
    [UIView animateWithDuration:0.5 animations:^{
        [self getCloseRightBar];
        [self pushAwayChatView];
    }];
}


/**
 *
 * Odsuwa tabelke z listą w prawo za ekran.
 *
 */
- (void)onGoingIntoFullMapView:(NSNotification *)notif
{
    [UIView animateWithDuration:0.5 animations:^{

        [self pushAwayRightBar];
        [self pushAwayChatView];
    }];
}
- (void)onGoingIntoChatView:(NSNotification *)notif
{
    [UIView animateWithDuration:0.5 animations:^{
        [self pushAwayRightBar];
        [self getCloseChatView];
    }];
}

- (void) pushAwayRightBar
{
    self.RightBar.frame = CGRectMake(
                                     self.view.frame.size.width,
                                     self.RightBar.frame.origin.y,
                                     self.RightBar.frame.size.width,
                                     self.RightBar.frame.size.height);
}
- (void) getCloseRightBar
{
    self.RightBar.frame = CGRectMake(
                                     self.view.frame.size.width /2,
                                     self.RightBar.frame.origin.y,
                                     self.RightBar.frame.size.width,
                                     self.RightBar.frame.size.height);
}
- (void) pushAwayChatView
{
    self.ChatView.frame = CGRectMake(
                                     -self.view.frame.size.width,
                                     self.ChatView.frame.origin.y,
                                     self.ChatView.frame.size.width,
                                     self.ChatView.frame.size.height);
}
- (void) getCloseChatView
{
    self.ChatView.frame = CGRectMake(
                                     0,
                                     self.ChatView.frame.origin.y,
                                     self.ChatView.frame.size.width,
                                     self.ChatView.frame.size.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGoingIntoPersonsView:) name: _N_intoPersonsView object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGoingIntoPersonsView:) name: _N_intoConversationsView object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGoingIntoFullMapView:) name: _N_intoFullMapView object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onGoingIntoChatView:) name: _N_intoChatView object:nil];
    
    
    //Ustawienie tabelki poza ekranem.
    [self onGoingIntoFullMapView:nil];
 
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
