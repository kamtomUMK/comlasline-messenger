//
//  BCCJSONConnector.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 23.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCCPerson.h"

@interface BCCJSONConnector : NSObject

+(id) getInstance;

-(BCCPerson *)getPersonFromServerById:(NSNumber*) userId;
-(void) getActiveFriendsAndActiveNeighborsAndSaveThemToCurrentlyLoggedUser;
-(void) updateVisibilityOnTheServerForCurrentLoggedUser;
-(int) getStatusForCurrentlyLoggedUserFromServer;
-(void) updateStatusForCurrentlyLoggedUserOnTheServer:(int)newStatus;
-(NSMutableArray *) getMyActiveChatPersonWindows;
-(BCCPerson *) getUserById:(int) userId;
-(NSMutableArray *) getMessageWithUser: (int)userId;
-(void) sendMessage:(NSString *)message toUser:(int) userId;
@end
