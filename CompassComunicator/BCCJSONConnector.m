//
//  BCCJSONConnector.m
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 23.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCJSONConnector.h"
#import "BCCPerson.h"
#import "BCCLoggedUser.h"

@implementation BCCJSONConnector

static BCCJSONConnector *myInstance;

-(id) init {
    if(!myInstance) {
        self = [super init];
        
        return self;
    }
    
    return myInstance;
}

+(id) getInstance {
    if(!myInstance) {
        myInstance = [[BCCJSONConnector alloc] init];
    }
    
    return myInstance;
}

-(NSDictionary *)doRequest:(NSString *)request{
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:request]];
    
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
}

-(BCCPerson *)getPersonFromServerById:(NSNumber*) userId {
    return nil;
}

-(int) getStatusForCurrentlyLoggedUserFromServer {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=get_status&user=%i", loggedUser.myPersonInstance.userId];
    NSDictionary *response = [self doRequest:request];
    
    return [[response valueForKey:@"status"] intValue];
}

-(void) updateStatusForCurrentlyLoggedUserOnTheServer:(int)newStatus {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=change_status&user=%i&status=%i", loggedUser.myPersonInstance.userId, newStatus];
    [self doRequest:request];
}

-(void) updateVisibilityOnTheServerForCurrentLoggedUser {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    int isShow = 0;
    
    if([loggedUser getMyVisibilityOnTheMap]) {
        isShow = 0;
    }
    else {
        isShow = 1;
    }
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=ustaw&user=%i&key=ja_na_mapie&value=%i", loggedUser.myPersonInstance.userId, isShow];
    [self doRequest:request];
}

-(NSMutableArray *) getMyActiveChatPersonWindows {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=info&user=%i&ts=1", loggedUser.myPersonInstance.userId];
    NSDictionary *response = [self doRequest:request];
    NSDictionary *openWindows = [response valueForKey:@"otwarte_okna"];
    
    NSMutableArray *activeChatPersonWindow = [[NSMutableArray alloc] init];
    
    for(NSNumber *userId in openWindows) {
        BCCPerson *person = [self getUserById:[userId intValue]];
        [activeChatPersonWindow addObject:person];
    }
    
    return activeChatPersonWindow;
}

-(BCCPerson *) getUserById:(int) userId {
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=userinfo&user=%i", userId];
    NSDictionary *response = [self doRequest:request];
    
    BCCPerson *retUser;
    for(NSDictionary *person in [response valueForKey:@"user"]) {
        retUser = [[BCCPerson alloc] initWithId:[[person valueForKey:@"user"] intValue]];
        
        retUser.image = [person valueForKey:@"image"];
        retUser.lat = [[person valueForKey:@"lat"] doubleValue];
        retUser.login = [person valueForKey:@"login"];
        retUser.lon = [[person valueForKey:@"lon"] doubleValue];
        retUser.sName = [person valueForKey:@"sname"];
        retUser.status = [[person valueForKey:@"status"] integerValue];
    }
    return retUser;
}

-(void) getActiveFriendsAndActiveNeighborsAndSaveThemToCurrentlyLoggedUser {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=list_of_friends&user=%i", loggedUser.myPersonInstance.userId];
    NSDictionary *response = [self doRequest:request];
    
    NSDictionary *friends = [response valueForKey:@"friends"];
    NSDictionary *otherVisiblePeople = [response valueForKey:@"pozostali"];
    
    
    loggedUser.friends = [[NSMutableArray alloc] init];
    loggedUser.otherVisiblePeople = [[NSMutableArray alloc] init];
    
    [self savePersonsFromDict:friends toList:loggedUser.friends];
    [self savePersonsFromDict:otherVisiblePeople toList:loggedUser.otherVisiblePeople];
}

-(void) savePersonsFromDict:(NSDictionary *)dict toList:(NSMutableArray *)list {
    for(NSDictionary *person in dict) {
        [list addObject:[self fillPersonFromDict:person]];
    }
}
-(BCCPerson *) fillPersonFromDict:(NSDictionary *)dict {
    BCCPerson *newPerson = [[BCCPerson alloc] initWithId:[[dict valueForKey:@"user"] intValue]];
    
    newPerson.image = [dict valueForKey:@"image"];
    newPerson.lat = [[dict valueForKey:@"lat"] doubleValue];
    newPerson.login = [dict valueForKey:@"login"];
    newPerson.lon = [[dict valueForKey:@"lon"] doubleValue];
    newPerson.distance = [[dict valueForKey:@"odl"] doubleValue];
    newPerson.sName = [dict valueForKey:@"sname"];
    newPerson.status = [[dict valueForKey:@"status"] integerValue];
    
    return newPerson;
}

-(NSMutableArray *) getMessageWithUser: (int)userId {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=info&user=%i&ts=1", loggedUser.myPersonInstance.userId];
    NSDictionary *response = [self doRequest:request];
    NSDictionary *messages = [response valueForKey:@"wiadomosci"];
    NSMutableArray *listOfMessages = [[NSMutableArray alloc] init];
    
    for (NSDictionary * item in messages) {
        if ([[item valueForKey:@"u_nadawca"] intValue] == userId || [[item valueForKey:@"u_adresat"] intValue] == userId) {
            [listOfMessages addObject:item];
        }
    }
    
    return listOfMessages;
}

-(void) sendMessage:(NSString *)message toUser:(int) userId {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    
    NSString *request = [NSString stringWithFormat:@"http://www.compas.org.pl/Chat/chatengine.jsp?op=dodaj&ts=1&nadawca=%i&odbiorca=%i&tresc=%@", loggedUser.myPersonInstance.userId, userId, message];
    
    [self doRequest:request];
}

@end
