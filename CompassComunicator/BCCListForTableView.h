//
//  BCCListForTableView.h
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCCListForTableView : NSObject <UITableViewDataSource>

@end
