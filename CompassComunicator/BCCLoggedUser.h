//
//  BCCLoggedUser.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 17.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCCPerson.h"

@interface BCCLoggedUser : NSObject
@property NSMutableArray *friends;
@property NSMutableArray *otherVisiblePeople;
@property BCCPerson *myPersonInstance;
@property NSMutableArray *openChatWindowPersons;

-(id) initWithUserId:(int)userId;
+(id) getInstance;

-(NSMutableArray *) getMyActiveFriends;
-(NSMutableArray *) getMyActiveNeighbors;
-(NSMutableArray *) getMyMessageWithUser : (int)userId;

-(BOOL) isMyFriend:(BCCPerson *)person;
-(BOOL) getMyVisibilityOnTheMap;
-(void) updateMyStatus:(int)newStatus;
-(int)  getMyStatus;
-(void) updateMyVisibility;
-(void) loadMyActiveFriendsAndActiveNeighbors;
-(NSMutableArray *) getMyActiveChatPersonWindows;
-(void) fillMyPersonInstance;
@end
