//
//  BCCLoggedUser.m
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 17.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCLoggedUser.h"
#import "BCCPerson.h"
#import "BCCJSONConnector.h"

@implementation BCCLoggedUser

static BCCLoggedUser *myInstance;

+(id) getInstance
{
    if (!myInstance)
    {
        myInstance = [[BCCLoggedUser alloc] init];
    }

    return myInstance;
}

-(id) init
{
    if (!myInstance)
    {
        self.myPersonInstance = [[BCCPerson alloc] init]; // userId uzupelniane w BCCViewController
        self.friends = [[NSMutableArray alloc] init];
        self.otherVisiblePeople = [[NSMutableArray alloc] init];
        
        return self;
    }
    
    return myInstance;
}

-(void) loadMyActiveFriendsAndActiveNeighbors {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    [jsonConnector getActiveFriendsAndActiveNeighborsAndSaveThemToCurrentlyLoggedUser];
}

-(NSMutableArray *) getMyActiveFriends {
    [self loadMyActiveFriendsAndActiveNeighbors];
    
    NSMutableArray *activeFriendsList = [[NSMutableArray alloc] init];
    
    for(BCCPerson *friend in self.friends) {
        if(friend.status == 3 || friend.status == 2) {
            [activeFriendsList addObject:friend];
        }
    }
    
    return activeFriendsList;
}

-(NSMutableArray *) getMyActiveNeighbors {
    [self loadMyActiveFriendsAndActiveNeighbors];
    
    NSMutableArray *activeNeighbors = [[NSMutableArray alloc] init];
    
    for(BCCPerson *activeNeighbor in self.otherVisiblePeople) {
        if(activeNeighbor.status == 3 || activeNeighbor.status == 2) {
            [activeNeighbors addObject:activeNeighbor];
        }
    }
    
    return activeNeighbors;
}

-(BOOL) isMyFriend:(BCCPerson *)person {
    if([self.friends containsObject:person]) {
        return YES;
    }
    else {
        return NO;
    }
}

-(BOOL)getMyVisibilityOnTheMap {
    
    return false;
}

/*
 ********
 *STATUS*
 ********
*/

/* WARTOSCI:
 * 0 - niedostepny
 * 1 - Niewidoczny
 * 2 - Dostepny tylko dla znajomych
 * 3 - Dostepny
*/

-(int) getMyStatus {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    self.myPersonInstance.status = [jsonConnector getStatusForCurrentlyLoggedUserFromServer];
    
    return self.myPersonInstance.status;
}

-(void) updateMyStatus:(int)newStatus {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    [jsonConnector updateStatusForCurrentlyLoggedUserOnTheServer:newStatus];
}

-(void) updateMyVisibility {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    [jsonConnector updateVisibilityOnTheServerForCurrentLoggedUser];
}

-(NSMutableArray *) getMyActiveChatPersonWindows {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    self.openChatWindowPersons = [jsonConnector getMyActiveChatPersonWindows];
    
    return self.openChatWindowPersons;
}

-(void) fillMyPersonInstance {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    self.myPersonInstance = [jsonConnector getUserById:self.myPersonInstance.userId];
}

-(NSMutableArray *) getMyMessageWithUser : (int)userId {
    BCCJSONConnector *jsonConnector = [BCCJSONConnector getInstance];
    
    return [jsonConnector getMessageWithUser:userId];
}

@end
