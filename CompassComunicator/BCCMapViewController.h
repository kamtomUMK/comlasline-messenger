//
//  BCCMapViewController.h
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 23.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CGTConnector.h"
#import "BCCConnDelegate.h"
#import <MapKit/MapKit.h>
#import "BCCGreenPeopleAnnotation.h"

@interface BCCMapViewController : UIViewController <MKMapViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *ChatView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIButton *showOrUnShowMeOnTheMapButton;

@property CGTConnector* conn;
@property NSMutableArray *activeFriends;
@property NSMutableArray *activeNeighbors;
@property NSMutableArray *activeFriendsAnnotations;
@property NSMutableArray *activeNeighborsAnnotations;
@property BCCGreenPeopleAnnotation *meAnnotation;
-(void)setConnector:(CGTConnector *)_conn;
@end
