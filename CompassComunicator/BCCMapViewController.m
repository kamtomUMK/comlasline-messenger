//
//  BCCMapViewController.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 23.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCMapViewController.h"
#import "BCCViewController.h"
#import "BCCLoggedUser.h"
#import "BCCBluePeopleAnnotation.h"
#import "BCCOrangePeopleAnnotation.h"
#import "BCCButton.h"
#import "BCCGreenPeopleAnnotation.h"
#import "BCCMenuBarViewController.h"
#import "BCCAnnotationView.h"
#import "BCCJSONConnector.h"

@interface BCCMapViewController()
@property BOOL _result;

@end
@implementation BCCMapViewController
@synthesize conn;
@synthesize _result;

- (void)updateMyVisibilityOnTheMap:(NSNotification *)notif {
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    [loggedUser updateMyVisibility];
    
    if([loggedUser getMyVisibilityOnTheMap]) {
        // zmiana obrazka OFF
        [self.mapView removeAnnotation:self.meAnnotation];
//        loggedUser.iAmVisibleOnTheMap = false;
    }
    else {
        //zmiana obrazka ON
        [self.mapView addAnnotation:self.meAnnotation];
  //      loggedUser.iAmVisibleOnTheMap = true;
    }
}

-(void)setConnector:(CGTConnector *)_conn{
    self.conn=_conn;
    [self.conn setDelegate:self];
}

-(void)didLoginWithResult:(BOOL)result{
    self._result=result;
    if (!_result){
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
-(void)viewDidLoad{
    self.activeFriends = [[NSMutableArray alloc] init];
    self.activeFriendsAnnotations = [[NSMutableArray alloc] init];
    
    [super viewDidLoad];
    _result=YES;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Disclosure Pressed" message:@"Click Cancel to Go Back" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    
    [alertView show];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return nil;
    }
    else if([annotation isKindOfClass:[BCCBluePeopleAnnotation class]]) {
        return [self prepareAnnotation:annotation WithImageName:@"man_blue.png" andMapView:mapView];
    }
    else if([annotation isKindOfClass:[BCCOrangePeopleAnnotation class]]) {
        return [self prepareAnnotation:annotation WithImageName:@"man_orange.png" andMapView:mapView];
    }
    else if([annotation isKindOfClass:[BCCGreenPeopleAnnotation class]]) {
        return [self prepareAnnotation:annotation WithImageName:@"green_people.png" andMapView:mapView];
    }

    return nil;
}

-(MKAnnotationView *) prepareAnnotation:(id <MKAnnotation>)annotation WithImageName:(NSString *)imageName andMapView:(MKMapView *) mapView {
    BCCAnnotationView *pinView;
    
    if (!pinView)
    {
        BCCPerson *person = ((BCCPeopleAnnotation *)annotation).person;
        pinView = [[BCCAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CustomPinAnnotation" AndPerson:person andImgName:imageName];
    }
    else {
        pinView.annotation = annotation;
    }
    return pinView;
}

-(void)viewDidAppear:(BOOL)animated{
    if (!_result) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateMyVisibilityOnTheMap:) name: _N_showMeOrUnshowMeOnTheMapView object:nil];
    
    self.mapView.delegate = self;
    self.mapView.mapType = MKMapTypeStandard;
    self.mapView.showsUserLocation = YES;
    
    [self showMyActiveFriendOnTheMap];
    [self showMyActiveNeighborOnTheMap];
    [self showMeOnTheMap];
}

-(void) showMyActiveFriendOnTheMap {
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    self.activeFriends = [loggedUser getMyActiveFriends];
    
    for(BCCPerson *activeFriend in self.activeFriends) {
        BCCBluePeopleAnnotation *activeFriendAnnotation = [[BCCBluePeopleAnnotation alloc] init];
        activeFriendAnnotation.coordinate = CLLocationCoordinate2DMake(activeFriend.lat, activeFriend.lon);
        activeFriendAnnotation.person = activeFriend;
        
        [self.mapView addAnnotation:activeFriendAnnotation];
        [self.activeFriendsAnnotations addObject:activeFriendAnnotation];
    }
}

-(void) showMyActiveNeighborOnTheMap {
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    self.activeNeighbors = [loggedUser getMyActiveNeighbors];
    
    for(BCCPerson *activeNeighbor in self.activeNeighbors) {
        BCCOrangePeopleAnnotation *activeNeighborAnnotation = [[BCCOrangePeopleAnnotation alloc] init];
        activeNeighborAnnotation.coordinate = CLLocationCoordinate2DMake(activeNeighbor.lat, activeNeighbor.lon);
        activeNeighborAnnotation.title = @" ";
        activeNeighborAnnotation.subtitle = @" ";
        activeNeighborAnnotation.person = activeNeighbor;

        [self.mapView addAnnotation:activeNeighborAnnotation];
        [self.activeNeighborsAnnotations addObject:activeNeighborAnnotation];
    }
}

-(void) showMeOnTheMap {
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    
    BCCGreenPeopleAnnotation *meAnnotation = [[BCCGreenPeopleAnnotation alloc] init];
    meAnnotation.coordinate = CLLocationCoordinate2DMake(loggedUser.myPersonInstance.lat, loggedUser.myPersonInstance.lon);
    meAnnotation.title = @" ";
    meAnnotation.subtitle = @" ";
    meAnnotation.person = loggedUser.myPersonInstance;
    
    [self.mapView addAnnotation:meAnnotation];
    self.meAnnotation = meAnnotation;
}
@end
