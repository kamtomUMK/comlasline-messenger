//
//  BCCMenuBarViewController.h
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define _N_intoPersonsView @"goingIntoPersonsView"
#define _N_intoFullMapView @"goingIntoFullMapView"
#define _N_intoConversationsView @"goingIntoConversationsView"
#define _N_showMeOrUnshowMeOnTheMapView @"showMeOrUnShowMeOnTheMapView"
#define _N_newViewDidAppear @"rightViewDidAppear"
#define _N_intoChatView @"goingIntoChatView"
@interface BCCMenuBarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *InnerView;
@end
