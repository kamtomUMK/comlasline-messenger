//
//  BCCMenuBarViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCMenuBarViewController.h"
#import "BCCLoggedUser.h"
#import "BCCJSONConnector.h"

@interface BCCMenuBarViewController ()
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;

@property (weak, nonatomic) IBOutlet UIButton *conversationsButton;
@property (weak, nonatomic) IBOutlet UIButton *personsButton;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;
@property (weak, nonatomic) IBOutlet UIButton *showMeOrUnShowMeOnTheMapViewButton;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@end

@implementation BCCMenuBarViewController
- (IBAction)logout:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)goingIntoConversationsView:(UIButton *)sender {
    
    //[self personsButton].enabled = true;
    //[self mapButton].enabled = true;
    //[self conversationsButton].enabled = false;
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName: _N_intoConversationsView
     object: self];
     
    /*
    [[NSNotificationCenter defaultCenter] postNotificationName: _N_intoChatView object: self];
     */
}

- (IBAction)goingIntoPersonsView:(UIButton *)sender {
    
    //[self personsButton].enabled = false;
    //[self mapButton].enabled = true;
    //[self conversationsButton].enabled = true;
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName: _N_intoPersonsView
                    object: self];
    
}

- (IBAction)goingIntoFullMapView:(UIButton *)sender {
    
    //[self personsButton].enabled = true;
    //[self mapButton].enabled = false;
    //[self conversationsButton].enabled = true;
    
    [[NSNotificationCenter defaultCenter]
      postNotificationName: _N_intoFullMapView
                    object: self];
}
- (IBAction)updateMyVisibility:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_showMeOrUnshowMeOnTheMapView object: self];
    
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    [loggedUser updateMyVisibility];
    
    if([loggedUser getMyVisibilityOnTheMap]) {
        [self.showMeOrUnShowMeOnTheMapViewButton setImage:[UIImage imageNamed:@"show_on_map_on.png"] forState:UIControlStateNormal];
    }
    else {
        [self.showMeOrUnShowMeOnTheMapViewButton setImage:[UIImage imageNamed:@"show_on_map_off.png"] forState:UIControlStateNormal];
    }
}
- (IBAction)showStatusAlert:(id)sender {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Zmien swoj status"
                                                      message:@""
                                                     delegate:self
                                            cancelButtonTitle:@"Powrót"
                                            otherButtonTitles:nil];
    
    [message addButtonWithTitle:@"Widoczny"];
    [message addButtonWithTitle:@"tylko dla znajomych"];
    [message addButtonWithTitle:@"niewidoczny"];
    [message addButtonWithTitle:@"niedostepny"];
    [message show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
    
    if(buttonIndex == 0) {
        NSLog(@"CANCEL");
    }
    else if(buttonIndex == 1) {
        [loggedUser updateMyStatus:3];
        [self.statusButton setImage:[UIImage imageNamed:@"status_online.png"] forState:UIControlStateNormal];
    }
    else if(buttonIndex == 2) {
        [loggedUser updateMyStatus:2];
        [self.statusButton setImage:[UIImage imageNamed:@"status_forfriends.png"] forState:UIControlStateNormal];
    }
    else if(buttonIndex == 3) {
        [loggedUser updateMyStatus:1];
        [self.statusButton setImage:[UIImage imageNamed:@"status_invisible.png"] forState:UIControlStateNormal];
    }
    else if(buttonIndex == 4) {
        [loggedUser updateMyStatus:0];
        [self.statusButton setImage:[UIImage imageNamed:@"status_offline.png"] forState:UIControlStateNormal];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[self logoutButton] sizeThatFits:CGSizeMake(10,10)];

    [self loadMyStatus];
    [self loadMyVisibilityOnTheMap];
}

-(void) loadMyStatus {
    BCCLoggedUser *loggedUser = [[BCCLoggedUser alloc] init];
    int status = [loggedUser getMyStatus];
    
    if(status == 0) {
        [self.statusButton setImage:[UIImage imageNamed:@"status_offline.png"] forState:UIControlStateNormal];
    }
    else if(status == 1) {
        [self.statusButton setImage:[UIImage imageNamed:@"status_invisible.png"] forState:UIControlStateNormal];
    }
    else if(status == 2) {
        [self.statusButton setImage:[UIImage imageNamed:@"status_forfriends.png"] forState:UIControlStateNormal];
    }
    else if(status == 3) {
        [self.statusButton setImage:[UIImage imageNamed:@"status_online.png"] forState:UIControlStateNormal];
    }
}

-(void) loadMyVisibilityOnTheMap {
    // POBRAC WIDOCZNOSC NA MAPIE!!!!!!!
    [self.showMeOrUnShowMeOnTheMapViewButton setImage:[UIImage imageNamed:@"show_on_map_on.png"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
