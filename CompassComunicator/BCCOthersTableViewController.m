//
//  BCCOthersTableViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 19.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCOthersTableViewController.h"
#import "BCCLoggedUser.h"
#import "BCCPersonsViewController.h"

@interface BCCOthersTableViewController ()

@property NSArray *MyActiveNeighbors;

@end

@implementation BCCOthersTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_TableViewDidAppear object:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//#warning Incomplete method implementation.
    self.MyActiveNeighbors = [[BCCLoggedUser getInstance] getMyActiveNeighbors];
    
    return self.MyActiveNeighbors.count;
    // Return the number of rows in the section.
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];*/
    
    // Configure the cell...
     UITableViewCell *cell = [[UITableViewCell alloc] init];
   
     [cell setFrame: CGRectMake(0,0,100, 30)];
     int i = [indexPath indexAtPosition:1];
      NSLog(@"komorka nr %d",i);
    
     BCCPerson* person = ((BCCPerson*)self.MyActiveNeighbors[i]);
     UILabel *lab =[[UILabel alloc]initWithFrame:CGRectMake(45, 0, 100, 30)];
     lab.font = [UIFont fontWithName:@"Arial" size:10];
     lab.text = [person login];
     
     UIImageView *avatar = [person getMyPicture];
     avatar.frame = CGRectMake(10,0,30,30);
     
     [cell addSubview: avatar];
     [cell addSubview: lab];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
