//
//  BCCPeopleAnnotation.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "BCCPerson.h"

@interface BCCPeopleAnnotation : MKPointAnnotation
@property BCCPerson *person;
@end
