//
//  BCCUser.h
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 17.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BCCPerson : NSObject
@property(nonatomic , retain)NSUserDefaults *standardUserDefaults;
@property(nonatomic,retain) NSNumber * timestamp;
@property(nonatomic, retain) NSMutableDictionary *windows;

@property int userId;
@property NSString *login;
@property NSString *sName;
@property int status;
@property NSString *country;
@property NSString *image;
@property double lat;
@property double lon;
@property double distance;

-(id) initWithId:(int) userId;
-(id) initByGettingNewFriendsWithId:(int) userId;
-(void) getMyFriends;
-(NSMutableArray *) getMyActiveFriends;
+(id) getInstance;
-(UIImageView *)getMyPicture;
@end
