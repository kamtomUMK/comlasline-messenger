//
//  BCCUser.m
//  CompassComunicator
//
//  Created by Dawid Skrzypczyński on 17.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCPerson.h"

@implementation BCCPerson
@synthesize standardUserDefaults;
@synthesize windows;
@synthesize timestamp;

-(id) initWithId:(int) userId {
    self = [super init];
    
    if (self){
        self.userId = userId;
    }
    self.timestamp=0;

    return self;
}

-(UIImageView *)getMyPicture {
    NSString *urlString = [NSString stringWithFormat:@"http://www.compas.org.pl/%@", self.image];
    
    UIImageView *myImageView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]]]];
    myImageView.frame = CGRectMake(5, 5, 35, 35);
    return myImageView;
}

@end
