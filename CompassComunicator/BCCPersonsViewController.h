//
//  BCCPersonsViewController.h
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import <UIKit/UIKit.h>

#define _N_intoFriendsTableView @"goingIntoFriendsTableView"
#define _N_intoOthersTableView @"goingIntoOthersTableView"
#define _N_intoInvitesTableView @"goingIntoInvitesTableView"
#define _N_TableViewDidAppear @"tableViewDidAppear"
@interface BCCPersonsViewController : UIViewController

@end
