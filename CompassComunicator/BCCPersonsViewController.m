//
//  BCCPersonsViewController.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCPersonsViewController.h"
#import "BCCMenuBarViewController.h"
#import "BCCLoggedUser.h"
#import "BCCListForTableView.h"
#import "BCCPerson.h"
@interface BCCPersonsViewController ()
@property bool isSegueAllowed;

@end

@implementation BCCPersonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_newViewDidAppear object:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onGoingIntoPersonsView:)
     name: _N_intoPersonsView
     object: nil ];
    
    [[NSNotificationCenter defaultCenter]
     addObserver: self
     selector: @selector(onGoingIntoConversationsView:)
     name: _N_intoConversationsView
     object: nil ];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
        selector:@selector(onRightViewDidAppear:)
            name:_N_newViewDidAppear
          object:nil];
    // Do any additional setup after loading the view.
}

- (IBAction)goingIntoOthersTableView:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_intoOthersTableView object:self];
}

- (IBAction)goingIntoFriendsTableView:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_intoFriendsTableView object:self];
}

- (IBAction)goingIntoInvitesTableView:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:_N_intoInvitesTableView object:self];
}

- (void)onRightViewDidAppear:(NSNotification *)notify
{
    [self setIsSegueAllowed: true];
}
- (void)onGoingIntoPersonsView:(NSNotification *)notify
{
    // NSLog(@"onGoingIntoPersonsView");
    if ([self isSegueAllowed] &&  [[self navigationController] visibleViewController] != self)
    {
        [self setIsSegueAllowed: false];
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
}

- (void)onGoingIntoConversationsView:(NSNotification *)notify
{
    // NSLog(@"onGoingIntoConversationsView");
    if ([self isSegueAllowed])
    {
        [self setIsSegueAllowed: false];
        [self performSegueWithIdentifier:@"goIntoConversationsView" sender:self];
    }
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
