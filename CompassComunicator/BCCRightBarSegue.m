//
//  BCCRightBarSegue.m
//  CompassComunicator
//
//  Created by Przemysław Porbadnik on 18.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCRightBarSegue.h"

@implementation BCCRightBarSegue
-(void) perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;

    // Add the destination view as a subview, temporarily
    [sourceViewController.view addSubview:destinationViewController.view];

    destinationViewController.view.frame =
    CGRectMake( 0, 0,
               sourceViewController.view.frame.size.width,
               sourceViewController.view.frame.size.height);
    
    
    destinationViewController.view.transform = CGAffineTransformMakeTranslation(-0.5,0);

    CGPoint originalCenter = destinationViewController.view.center;

    destinationViewController.view.center =
    CGPointMake( sourceViewController.view.frame.size.width,
                (destinationViewController.view.frame.size.height)/ 2);

    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         destinationViewController.view.center = originalCenter;
                     }
                     completion:^(BOOL finished){
                         [sourceViewController.navigationController pushViewController:destinationViewController animated:NO];
                     }];
}
@end
