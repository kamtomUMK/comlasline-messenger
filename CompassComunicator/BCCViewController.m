//
//  BCCViewController.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 15.04.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "BCCViewController.h"
#import "BCCMapViewController.h"
#import "BCCPerson.h"
#import "BCCLoggedUser.h"
#import "BCCJSONConnector.h"

@interface BCCViewController ()
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIView *MainView;

@end

@implementation BCCViewController
@synthesize conn;
@synthesize password;
@synthesize email;
- (void)viewDidLoad
{
    [super viewDidLoad];
    conn = [[CGTConnector alloc]init];
	[conn setDelegate: self];
    self.isLoginFinished = NO;
   
    
}
- (IBAction)Login:(id)sender {
    [conn startConnectionWithLogin:email.text andPassword:password.text];
    
}
- (IBAction)StartEditing:(id)sender {
    [UIView animateWithDuration:1.0 animations:^{
        self.MainView.center=CGPointMake(160, self.view.center.y-150);
    }];
}

-(void)viewWillAppear:(BOOL)animated {
    password.text= [[NSUserDefaults standardUserDefaults] objectForKey:@"_58_password"];
    email.text= [[NSUserDefaults standardUserDefaults] objectForKey:@"_58_emailAddress"];
    
    [conn clearCookies];
     [conn checkInfo];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqual:@"login"]){
        BCCMapViewController* bv  =(BCCMapViewController*) [segue destinationViewController];
        [bv setConnector:conn];
        [conn startConnectionWithLogin:email.text andPassword:password.text];
    }
}
-(void)didLoginWithResult:(BOOL) result andResponseText:(NSString *)response {
    if (result){
        if(self.isLoginFinished == NO) {
            // inicjacja uzytkownika w systemie.
            int userid=[[[[[response componentsSeparatedByString:@"var SIGNED_USER="] objectAtIndex:1] componentsSeparatedByString:@";"] objectAtIndex:0] intValue];
            
            BCCLoggedUser *loggedUser = [BCCLoggedUser getInstance];
            loggedUser.myPersonInstance.userId = userid;
            [loggedUser fillMyPersonInstance];
            [loggedUser loadMyActiveFriendsAndActiveNeighbors];
            
            self.isLoginFinished = YES;
        }
        [self performSegueWithIdentifier:@"loggedin" sender:self];
    }
}



@end
