//
//  CGTChatCOntroller.h
//  Compas
//
//  Created by Błażej Zyglarski on 02.10.2013.
//  Copyright (c) 2013 BEDESIGN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
@interface CGTChatCOntroller : NSObject

@property(nonatomic,retain) NSNumber * userid;
@property(nonatomic , retain)NSUserDefaults *standardUserDefaults;
@property(nonatomic,retain) NSNumber * timestamp;
@property(nonatomic,retain) NSMutableDictionary * windows;

@property (weak, nonatomic)  MKMapView *vi;
-(void)orderWindows;
-(void)check;

-(void)getMessage:(NSDictionary*)wiadomosc;


@end
