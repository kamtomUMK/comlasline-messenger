//
//  CGTConnector.h
//  Compas
//
//  Created by Błażej Zyglarski on 24.08.2013.
//  Copyright (c) 2013 BEDESIGN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BCCConnDelegate.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"

@interface CGTConnector : NSObject
@property(nonatomic, retain) NSURLConnection * conn;
@property(nonatomic, retain) NSMutableString * _responseData;

-(void)startConnectionWithLogin:(NSString*)login andPassword:(NSString*)password;
-(void)checkInfo;

-(UIImage*)captchaNewAccount:(NSString*)pAuth;
-(NSString*)pAuthNewAccount;

-(void)setDelegate:(id <BCCConnDelegate>)del;

-(BOOL)sendData:(NSMutableDictionary*)data to:(NSString*)target target:(id)t selector:(SEL)s;
-(NSString* )stripDataInsideTag:(NSString*)tag from:(NSString*)resp;
-(NSString* )findLastMessageOfType:(NSString*)type in:(NSString*)resp;
-(void)clearCookies;
@end
