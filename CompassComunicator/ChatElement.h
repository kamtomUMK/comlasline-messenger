//
//  ChatElement.h
//  Compas
//
//  Created by Błażej Zyglarski on 02.10.2013.
//  Copyright (c) 2013 BEDESIGN. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <QuartzCore/QuartzCore.h>
@interface ChatElement : UIView
@property(nonatomic ,retain)NSNumber * uid;
@property(nonatomic ,retain) NSMutableDictionary* wiadomosci;
@property(nonatomic,retain) NSNumber * suid;
@property(nonatomic,retain) UILabel * ilosc;

@property(atomic) BOOL dtop;

-(id)initWithName:(NSString*)name;

-(void)putWiadomosc:(NSDictionary*)w;
@end
