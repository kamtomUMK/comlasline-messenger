//
//  ChatElement.m
//  Compas
//
//  Created by Błażej Zyglarski on 02.10.2013.
//  Copyright (c) 2013 BEDESIGN. All rights reserved.
//

#import "ChatElement.h"
#import "NSBubbleData.h"
#import "UIBubbleTableView.h"
#import "CGTChatCOntroller.h"
#import "CGTMapViewControler.h"

@implementation ChatElement

@synthesize uid;
@synthesize suid;
@synthesize dtop;
@synthesize wiadomosci;
@synthesize ilosc;
@synthesize table;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(IBAction)handleTap:(UIPanGestureRecognizer *)recognizer{
    NSLog(@"ZMINAA ROZMIARU");
    [UIView beginAnimations:@"button_in" context:nil];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDone)];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationBeginsFromCurrentState:YES];
    if (dtop){
        dtop = NO;
        [self setFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y+300,330,50)];
    }else{
        dtop = YES;
        [self setFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y-300,330,350)];
        self.ilosc.text=@"0";
    }
    CGTMapViewControler * s = (CGTMapViewControler *)self.superview.superview.nextResponder;
    [s.cline orderWindows];
    //     [s orderWindows ];
    [UIView commitAnimations];
    
}

- (id)initWithName:(NSString*)name
{
    CGRect frame = CGRectMake(0,600,330,50);
    dtop=NO;
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithRed:0.9 green:.9 blue:.9 alpha:.9]];
        [self setClipsToBounds:YES];
        self.ilosc = [[UILabel alloc]initWithFrame:CGRectMake(250, 15, 35, 15)];
        self.ilosc.text=@"0";
        [self.ilosc setBackgroundColor:[UIColor clearColor]];
        [self.ilosc setFont:[UIFont systemFontOfSize:14.0]];
        [self addSubview:self.ilosc];
        
        UILabel *titl = [[UILabel alloc]initWithFrame:CGRectMake(15, 15, 200, 15)];
        [titl setBackgroundColor:[UIColor clearColor]];
        [titl setFont:[UIFont systemFontOfSize:14.0]];
        titl.text=name;
        [self addSubview:titl];
        
        
        self.wiadomosci=[NSMutableDictionary dictionaryWithCapacity:0];
        
        
        UIButton * show = [UIButton buttonWithType:UIButtonTypeContactAdd];
        [show setFrame:CGRectMake(300, 15, 15, 15)];
        UITapGestureRecognizer *g = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self action:@selector(handleTap:)];

        [show addGestureRecognizer:g];
        [self addSubview:show];
        table= [[UIBubbleTableView alloc] initWithFrame:CGRectMake(0,50,300,300)];
        [self addSubview:table];
        table.layer.shadowOffset = CGSizeMake(0, 0);
        table.layer.shadowRadius = 20;
        table.layer.shadowOpacity = 0.5;
        
        [table setBubbleDataSource:self];
    }
    return self;
}

-(void)putWiadomosc:(NSDictionary*)w{

    
    NSString *idwpis = [w valueForKey:@"id_wpis"] ;
    if ([self.wiadomosci valueForKey:idwpis]==nil){
        [self.wiadomosci setObject:w forKey:idwpis];
        if(self.dtop==NO){
            int i = [self.ilosc.text intValue]+1;
            self.ilosc.text=[NSString stringWithFormat:@"%d",i ];
        }
    
    [self.table reloadData];
    [self.table scrollBubbleViewToBottomAnimated:YES];
    }
}

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView{
    NSLog(@"ZWRACANIE LICZBY WIESZY:%d",self.wiadomosci.count);
    return self.wiadomosci.count;
}
- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row{
    
    
    NSArray *sortedKeys = [[self.wiadomosci allKeys] sortedArrayUsingSelector: @selector(compare:)];

    
    NSDictionary* w=[self.wiadomosci objectForKey:[sortedKeys objectAtIndex:row]];
    NSBubbleData * n = [NSBubbleData dataWithText:[w valueForKey:@"wpis"] date:[[NSDate alloc]init] type: (self.uid==self.suid)?BubbleTypeMine:BubbleTypeSomeoneElse];
    return n;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
