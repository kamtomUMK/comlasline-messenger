//
//  UILabel+HtmlLabel.m
//  CompassComunicator
//
//  Created by Błażej Zyglarski on 11.05.2014.
//  Copyright (c) 2014 BZC. All rights reserved.
//

#import "UILabel+HtmlLabel.h"

@implementation UILabel (HtmlLabel)
- (void) setHtml: (NSString*) html
{
    NSError *err = nil;
    
    NSAttributedString * info2=  [[NSAttributedString alloc] initWithData:[[NSString stringWithFormat:@"<div style='color:white;text-align:center;font-size:15px;font-family:System,Tahoma,Arial'>%@</div>",html] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:&err];
    self.attributedText =info2;
    
    if(err)
        NSLog(@"Unable to parse label text: %@", err);
}

@end
